module Main where

import XMonad
import XMonad.Config.Xfce
import XMonad.Config.Desktop
import XMonad.Layout.Spacing
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.SetWMName
import XMonad.Actions.PhysicalScreens
import XMonad.Util.EZConfig(additionalKeys)

myManageHook :: [ManageHook]
myManageHook =
  [
    className =? "Xfrun4" --> doFloat
  , className =? "Skype" --> doFloat
  , className =? "KeePaasXC" --> doFloat
  ]

myLayout = tiled ||| Mirror tiled ||| Full
  where
    tiled   = spacing 5 $ Tall nmaster delta ratio
    nmaster = 1
    delta   = 5/100
    ratio   = 1/2

myKeys =
  [
    -- ((mod4Mask               , xK_q), restart "xmonad" True)
    ((mod4Mask .|. shiftMask , xK_q), spawn "xfce4-session-logout")
  ]
  ++
  [
    ((mod4Mask .|. mask, key), f sc)
      | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..],
        (f, mask) <- [(viewScreen, 0), (sendToScreen, shiftMask)]
  ]

myWorkspaces =
  (map show [0..9]) ++ ["-", "="]

main :: IO ()
main = xmonad $ xfceConfig
  { terminal    = "terminator"
  , startupHook = setWMName "LG3D"
  , modMask     = mod4Mask
  , manageHook  = composeAll myManageHook <+> manageHook xfceConfig
  , layoutHook  = desktopLayoutModifiers myLayout
  , workspaces  = myWorkspaces
  } `additionalKeys` myKeys
